# egg配置

## 基础配置

```js
$ npm i egg --save
$ npm i egg-bin --save-dev
# npm i egg-view-nunjucks --save // vue模版插件

```

## 静态资源

Egg 内置了 static 插件，线上环境建议部署到 CDN，无需该插件。
static 插件默认映射 /public/*-> app/public/* 目录

```js
exports.view = {
  defaultViewEngine: 'nunjucks', // 设置了默认的视图引擎为 Nunjucks，即当我们在项目中使用 res.render() 方法渲染模板时，会默认使用 Nunjucks 引擎进行渲染
  mapping: {
    '.tpl': 'nunjucks', // 通过 mapping 属性将 .tpl 后缀名与 Nunjucks 引擎进行映射，表明当我们在代码中设置模板文件的后缀名为 .tpl 时，也会使用 Nunjucks 引擎进行渲染。这样做的好处是可以让我们在项目中使用不同的模板引擎，而不必更改大量代码。
  },
};

```

## Controller

框架推荐 Controller 层主要对用户的请求参数进行处理（校验、转换），然后调用对应的 service 方法处理业务，得到业务结果后封装并返回

```js
const Controller = require('egg').Controller

// 定义了一个控制器类 NewsController，其中的 list() 方法用于渲染 'news/list.tpl' 模板并将 dataList 数据传递给模板。通过 Egg.js 提供的框架特性，实现了快速构建可扩展的应用程序。
class NewsController extends Controller {
  // list() 方法是一个异步方法， 该方法用于处理某个路径下的请求，并返回相应的数据或渲染某个模板
  async list() {
    const ctx = this.ctx
    // 获取查询参数 page 的值并赋值给变量
    const page = ctx.query.page || 1
    // newsList={"list": []}
    const newsList = await ctx.service.news.list(page)

    // 1、返回数据
    ctx.body = {
      message: 'Hello, Egg!',
      data: data,
    }
    // 调用 News 服务中的 list() 方法，获取指定页数的新闻列表，并将结果赋值给变量 newsList。由于 list() 方法返回一个 Promise 对象，所以需要使用 await 关键字等待该异步操作完成。

    // 2、直接渲染模版
    await this.ctx.render('news/list.tpl', newsList)
    // 通过 this.ctx.render() 方法调用模板引擎来渲染模板。第一个参数是模板文件的路径，这里是 'news/list.tpl'，表示渲染位于 views 文件夹下的 news/list.tpl 模板文件。第二个参数是要传递给模板的数据，这里是 dataList。
  }
}

```

```js
  const createRule = {
      title: { type: 'string' },
      content: { type: 'string' },
    };
    // ctx.validate(createRule) 的作用是对请求参数进行校验，确保参数中包含 title 和 content 字段，并且它们的类型都是字符串。
    ctx.validate(createRule);
```

## Middleware

```JS
// app/middleware/gzip.js
// app/middleware/gzip.js
const isJSON = require('koa-is-json');
const zlib = require('zlib');

module.exports = (options) => {
  // options === app.config.gzip
  return async function gzip(ctx, next) {
    await next();

    // 后续中间件执行完成后将响应体转换成 gzip
    let body = ctx.body;
    if (!body) return;

    // 支持 options.threshold
    if (options.threshold && ctx.length < options.threshold) return;

    if (isJSON(body)) body = JSON.stringify(body);

    // 设置 gzip body，修正响应头
    const stream = zlib.createGzip();
    stream.end(body);
    ctx.body = stream;
    ctx.set('Content-Encoding', 'gzip');
  };
};
```

### 配置[match](https://www.eggjs.org/zh-CN/basics/middleware#match-%E5%92%8C-ignore)，规定中间件适用于那些匹配的url

```js
// config/config.default.js
module.exports = {
  // 配置需要的中间件，数组顺序即为中间件的加载顺序
  middleware: ['gzip'],

  // 配置 gzip 中间件的配置
  gzip: {
     match: '/static',
    threshold: 1024, // 小于 1k 的响应体不压缩
  },
};
```

## Router

### 调用模版

```JS
router.verb('router-name', 'path-match', middleware1, ..., middlewareN, app.controller.action);

```

### params路由模式匹配

```js
// http://127.0.0.1:7002/news/id=11 匹配
// app/router.js
module.exports = (app) => {
  const { router, controller } = app;
  router.get('/user/:id', controller.user.info);
};

// app/controller/user.js
class UserController extends Controller {
  async info() {
    const { ctx } = this;
    ctx.body = {
      name: `hello ${ctx.params.id}`, // ctx.params.id
    };
  }
}
```

### 注意事项

在 Router 定义中， 可以支持多个 Middleware 串联执行
Controller 必须定义在 app/controller 目录中。
一个文件里面也可以包含多个 Controller 定义，在定义路由的时候，可以通过 ${fileName}.${functionName} 的方式指定对应的 Controller。
Controller 支持子目录，在定义路由的时候，可以通过 ${directoryName}.${fileName}.${functionName} 的方式指定对应的 Controller。

## Service

### [Service ctx 详解](https://www.eggjs.org/zh-CN/basics/service#service-ctx-%E8%AF%A6%E8%A7%A3)

+ 使用 this.ctx.curl 发起网络调用。
+ 通过 this.ctx.service.otherService 调用其他 Service。
+ 调用 this.ctx.db 发起数据库操作，db 可能是插件预挂载到 app 上的模块。
