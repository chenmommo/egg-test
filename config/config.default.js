exports.keys = '111' // Please set config.keys first
// Node.js 的模板渲染配置，使用的是 Nunjucks 模板引擎
exports.view = {
  defaultViewEngine: 'nunjucks', // 设置了默认的视图引擎为 Nunjucks，即当我们在项目中使用 res.render() 方法渲染模板时，会默认使用 Nunjucks 引擎进行渲染
  mapping: {
    '.tpl': 'nunjucks', // 通过 mapping 属性将 .tpl 后缀名与 Nunjucks 引擎进行映射，表明当我们在代码中设置模板文件的后缀名为 .tpl 时，也会使用 Nunjucks 引擎进行渲染。这样做的好处是可以让我们在项目中使用不同的模板引擎，而不必更改大量代码。
  },
};

exports.json = {
  dir: '/json',
};

exports.news = {
  pageSize: 5,
  serverUrl: 'https://hacker-news.firebaseio.com/v0',
}

// // add middleware robot
exports.middleware = ['robot', 'gzip'];
// robot's configurations
exports.robot = {
  ua: [/Baiduspider/i],
};

exports.gzip = {
  //  gzip 只针对 /static 前缀开头的 url 请求开启
  match: '/static',
  threshold: 10, // 小于 1k 的响应体不压缩
}
