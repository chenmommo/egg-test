// app/router.js
module.exports = (app) => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  const gzip = app.middleware.gzip({ threshold: 1024 });
  // router 中使用中间件
  router.get('/news/:id', gzip, controller.news.list);
};

