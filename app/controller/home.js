/*
 * @Author: liuliuliu 2249284602@qq.com
 * @Date: 2023-11-06 17:45:00
 * @LastEditors: liuliuliu 2249284602@qq.com
 * @LastEditTime: 2023-11-06 17:52:01
 * @FilePath: /egg/app/controller/home.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// app/controller/home.js
const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    this.ctx.body = 'Hello world!';
  }
}

module.exports = HomeController;
