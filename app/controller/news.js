const Controller = require('egg').Controller

// 定义了一个控制器类 NewsController，其中的 list() 方法用于渲染 'news/list.tpl' 模板并将 dataList 数据传递给模板。通过 Egg.js 提供的框架特性，实现了快速构建可扩展的应用程序。
class NewsController extends Controller {
  // list() 方法是一个异步方法， 该方法用于处理某个路径下的请求，并返回相应的数据或渲染某个模板
  async list() {
    const ctx = this.ctx
    const page = ctx.query.page || 1
    console.log(page)
    const data = await ctx.service.news.list(page)
    ctx.body = {
      message: `Hello, Egg!${ctx.params.id}`,
      data: data,
    }
    /** ctx不直接渲染
    const newsList = await ctx.service.news.list(page) // 调用 News 服务中的 list() 方法，获取指定页数的新闻列表，并将结果赋值给变量 newsList。由于 list() 方法返回一个 Promise 对象，所以需要使用 await 关键字等待该异步操作完成。
    await ctx.render('news/list.tpl', data)
    */
    // 通过 this.ctx.render() 方法调用模板引擎来渲染模板。第一个参数是模板文件的路径，这里是 'news/list.tpl'，表示渲染位于 views 文件夹下的 news/list.tpl 模板文件。第二个参数是要传递给模板的数据，这里是 dataList。
  }
}

module.exports = NewsController;
